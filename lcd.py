from RPLCD.i2c import CharLCD
import state
import channel
import time
import sys

# note: displays appear to work reliably so far up to 200 kbaud of i2c baud rate.
# i2c baud rate of 400 kbaud showed occasional signs of error (junk on screen) 
class LCD:
    def __init__(self,addr,channel_idx):
        self.addr = addr
        self.lcd = CharLCD(i2c_expander='PCF8574', address=addr, port=1,
                           cols=20, rows=4, dotsize=8,
                           charmap='A02',
                           auto_linebreaks=True,
                           backlight_enabled=True)
        self.lcd.clear()
        self.write_lcd('OrcaPID8 v1.1\r\n\r\nStarting up, please wait...')
        self.channel_idx = channel_idx # this should be either 1 or 5
        
    def write_lcd(self, string):
        try:
            self.lcd.write_string(string)
        except KeyboardInterrupt:
            print ('Interrupted')
            sys.exit(0)
        except:
            print('LCD comms error. Trying to restart i2c...')
            del self.lcd
            time.sleep(1)
            
            self.__init__(self.addr,self.channel_idx)
            self.write_lcd(string)
            print('  success')
        
    def format_temp(self,temp):
        if temp == -1:
            return '  -'
        else:
            return '%3d' % int(min(max(round(temp),0),999))
                
    # version of update which shows 2 TC readings per channel
    def update2(self,TCA,TCB,setpoint,output):
        self.lcd.home()
        

        TCAstr = [self.format_temp(temp) for temp in TCA]        
        TCBstr = [self.format_temp(temp) for temp in TCB]
        setstr = [self.format_temp(temp) for temp in setpoint]
        outstr = []
        for o in output:
            if o < 0:
                outstr.append('OFF')
            elif o >= 0 and o <= 100:
                outstr.append('%3d' % int(round(o)))
            else:
                outstr.append('ERR')
         

 
        lcdstr =  'TCA  ' + ' '.join(TCAstr) + '\n\r'
        lcdstr += 'TCB  ' + ' '.join(TCBstr) + '\n\r'
        lcdstr += 'SetT ' + ' '.join(setstr) + '\n\r'
        lcdstr += 'Out% ' + ' '.join(outstr)
        self.write_lcd(lcdstr)
        
    # version which shows only one TC reading per channel
    def update1(self,current,setpoint,output):
        self.lcd.home()
        
        curstr = [self.format_temp(temp) for temp in current]   
        setstr = [self.format_temp(temp) for temp in setpoint]
        outstr = []
        for o in output:
            if o < 0:
                outstr.append('OFF')
            elif o >= 0 and o <= 100:
                outstr.append('%3d' % int(round(o)))
            else:
                outstr.append('ERR')
         

 
        lcdstr =  '     ' + ' '.join([('Ch%d' % (ch+self.channel_idx)) for ch in range(0,4)]) + '\n\r'
        lcdstr += 'CurT ' + ' '.join(curstr) + '\n\r'
        lcdstr += 'SetT ' + ' '.join(setstr) + '\n\r'
        lcdstr += 'Out% ' + ' '.join(outstr)
        self.write_lcd(lcdstr)
    
    def close(self):
        self.lcd.close()
        
        

class Displays:
    def __init__(self,addr1,addr2,show_both_TCs=True):
        self.addr1 = addr1
        self.addr2 = addr2
        self.lcd1 = LCD(addr1,1)
        self.lcd2 = LCD(addr2,5)
        self.show_both_TCs = show_both_TCs
        
    def update(self,channels):
        TCA = []
        TCB = []
        setpoint = []
        output = []

        for i in range(len(channels)):
            channel = channels[i]
            
            this_TCA = -1
            if channel.state.TCA["fault"] == -1: # no fault, so display T value
                this_TCA = channel.state.TCA["ext"]
                
            this_TCB = -1
            if channel.state.TCB["fault"] == -1: # no fault, so display T value
                this_TCB = channel.state.TCB["ext"]
                
            TCA.append(this_TCA)
            TCB.append(this_TCB)
            setpoint.append(channel.state.command.setpoint)
            
            if channel.state.hw_enable and channel.state.command.sw_enable:
                this_output = channel.state.output
            else:
                this_output = -1
            output.append(this_output)
        if self.show_both_TCs:
            self.lcd1.update2(TCA[0:4],TCB[0:4],setpoint[0:4],output[0:4])
            self.lcd2.update2(TCA[4:8],TCB[4:8],setpoint[4:8],output[4:8])
        else:
            self.lcd1.update1(TCA[0:4],setpoint[0:4],output[0:4])
            self.lcd2.update1(TCA[4:8],setpoint[4:8],output[4:8])            
