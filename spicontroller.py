import spidev 
import RPi.GPIO as GPIO
import time



class SPIController:
    def __init__(self):
        # edit 09-03-2021: dropped speed from 100 kHz to 40 kHz
        # to try to combat occasional spells of noisy readings
        self.spi_speed = 50000  # in Hz
        # CD74HC154M96 pins
        self.output_pins = {  "ssb0": 23, # b0 for 4-bit address decoder CD74HC154M96
                              "ssb1": 24,
                              "ssb2": 25,
                              "ssb3": 6,
                              "ssclr": 8, # SPI CE0; goes to address decoder
                              "tcs": 27, # select A/B thermocouple
                              "lsen": 16 # level shifter enable 
                            }
        self.input_pins = { "enc": 5 # input from 8:1 mux
                            }
        
    
        
        
        # set up GPIO chip select pins
        GPIO.setmode(GPIO.BCM)
        for name,pin in self.output_pins.items():
            #print ('setting pin GPIO%d to output mode (%s)' % (pin,name))
            GPIO.setup(pin, GPIO.OUT, initial=1)
        for name,pin in self.input_pins.items():
            #print ('setting pin GPIO%d to input mode (%s)' % (pin,name))
            GPIO.setup(pin, GPIO.IN)
            
        # set up SPI
        self.spi = spidev.SpiDev()
        self.spi.open(0, 0) # open on CE0
        self.spi.max_speed_hz = self.spi_speed
        
        # lsen should always be 1
        GPIO.output(self.output_pins['lsen'], 1)
        
    def select_channel(self, channel):
        for i in range(3):
            GPIO.output(self.output_pins['ssb'+str(i)], (channel&(2**i))>>i)
            
    def select_dacs(self, dacs=True):
        GPIO.output(self.output_pins['ssb3'], dacs)
        
    def select_tcb(self, b=True):
        GPIO.output(self.output_pins['tcs'], b)
            
    def to_float(self, data, decimal):
        return int(data>>decimal) + float(2**(-decimal)*(data&(2**decimal-1)))
    
    def select_bits(self, data, start, end):
        return (data&(((2**(start+1)-1)>>end)<<end))>>end
    
    def read_MAX31855(self):
        reply_byte_list = self.spi.readbytes(4)#self.spi.xfer([0,0,0,0])

        reply = (reply_byte_list[0]<<24) + (reply_byte_list[1]<<16) + (reply_byte_list[2]<<8) + reply_byte_list[3]
        
        external = self.to_float(self.select_bits(reply,30,18),2)*(-1)**((2**31)&reply) 
        internal = self.to_float(self.select_bits(reply,14,4),4)*(-1)**((2**15)&reply) 
        fault = (2**16)&reply
        fault_code = 0b111&reply
        return external,internal,fault,fault_code

           
    def write_MCP4821(self, voltage):
        # using gain 1 setting, so voltage = 2.048*vbits/4096
        vbits = int(round(4096*abs(voltage)/2.048))
        data = (0b0011<<12) | vbits
        
        # 0010 - 12 bits
        self.spi.xfer([data>>8,self.select_bits(data,7,0)])

    def read_enc(self):
        return GPIO.input(self.input_pins['enc'])
        
    def update_channel(self, channel, voltage):
        # edit 09-03-21: adding voodoo sleeps here to see if it
        # helps with what appears to be occasional digital data
        # transfer issues
        time.sleep(0.002)
        self.select_channel(channel)
        
        # first read thermocouple A
        self.select_dacs(False)
        self.select_tcb(False)
        time.sleep(0.002)
        ext_A,int_A,fault_A,fault_code_A = self.read_MAX31855()
        fault_output_A = -1
        if fault_A:
            fault_output_A = fault_code_A
            #print("warning! MAX31855 ch %d A reporting fault code %d" % (channel,fault_code_A))
        time.sleep(0.002)
        # now thermocouple B
        self.select_tcb(True)
        time.sleep(0.002)
        ext_B,int_B,fault_B,fault_code_B = self.read_MAX31855()
        fault_output_B = -1
        if fault_B:
            fault_output_B = fault_code_B
            #print("warning! MAX31855 ch %d B reporting fault code %d" % (channel,fault_code_B))
        
        
        time.sleep(0.002)
        # read input
        enc = self.read_enc()
        
        time.sleep(0.002)
        # now dacs
        self.select_dacs(True)
        time.sleep(0.002)
        self.write_MCP4821(voltage)
        
        time.sleep(0.002)
        return ext_A, int_A, fault_output_A, ext_B, int_B, fault_output_B, enc

    def close(self):
        spi.close()
        
        
#s = SPIController()
#time.sleep(0.1)
#s.update_channel(0,0.5)
#s.spi.close()
#GPIO.setup(10, GPIO.OUT, initial=1)
#GPIO.setup(11, GPIO.OUT, initial=1)
