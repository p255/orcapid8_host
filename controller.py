import time
import state
import spicontroller
import channel
import fileio
import time
from lcd import Displays

state_filename = "pkl/state.pkl"
command_filename = "pkl/cmd.pkl"

spi = spicontroller.SPIController()
channels = [channel.Channel(i,spi) for i in range(1,9)]
time.sleep(0.001)
displays = Displays(0x26,0x27,show_both_TCs=True)
time.sleep(0.001)

fileio.save([channel.state.command for channel in channels],command_filename)

try:
    while True:
        commands = fileio.load(command_filename)
        
        for i in range(len(channels)):
            channels[i].update(commands[i])
            #time.sleep(0.2)
            
        time.sleep(0.001)
        displays.update(channels)
        
        time.sleep(0.001)
        fileio.save([channel.state for channel in channels],state_filename)
        
except KeyboardInterrupt:
    print('\nuser terminated\n')
