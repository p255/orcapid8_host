import time
import numpy as np

class PID:
    def __init__(self):
        self.Kp = 0
        self.Ki = 0
        self.Kd = 0
        self.output = 0
        self.output_P = 0
        self.output_I = 0
        self.output_D = 0
        self.setpoint = 0
        self.temperature = 0
        self.error_integral = 0
        self.history_len = 100
        self.temperature_history = [0]
        self.timestamps = [time.time()]
        self.min_output = 0
        self.max_output = 100
        self.min_error_integral = -5
        self.enabled = True
        self.startup_counter = 0
        
    def update_params(self, state):
        self.Kp = state.command.Kp
        self.Ki = state.command.Ki
        self.Kd = state.command.Kd
        self.setpoint = state.command.setpoint
        self.max_output = state.command.max_output
        self.temperature = state.TCA["ext"] # using TCA for now
        # we'll consider this PID disabled if fault or if the temperature exceeds 600C (presumably error)
        self.enabled = (state.hw_enable and state.command.sw_enable
                        and (self.temperature<600) and state.TCA["fault"]==-1)
        
        
    def step(self, state):
        self.update_params(state)
        
        this_timestamp = time.time()
        if self.startup_counter < 3:
            self.timestamps[0] = this_timestamp
            self.temperature_history[0] = self.temperature
            self.startup_counter += 1
            return self.output
        
        this_interval = this_timestamp - self.timestamps[0]
        if len(self.timestamps) < self.history_len:
            self.timestamps = [this_timestamp] + self.timestamps
            self.temperature_history = [self.temperature] + self.temperature_history
        else:
            self.timestamps = [this_timestamp] + self.timestamps[0:len(self.timestamps)-1]
            self.temperature_history = [self.temperature] + self.temperature_history[0:len(self.temperature_history)-1]
            
        
        if self.enabled:
            this_error = self.setpoint-self.temperature
            
            P = self.Kp*this_error
            
            # only accumulate error_integral if output is not clamped
            if ((self.output < (self.max_output-1e-4) or this_error < 0)
                and (self.output > (self.min_output+1e-4) or this_error > 0)):
                self.error_integral += self.Ki*this_error*this_interval

            self.error_integral = max(min(self.error_integral, self.max_output),self.min_error_integral)
            I = self.error_integral
            
            if len(self.temperature_history) == self.history_len:
                D = -self.Kd*np.average(np.gradient(np.array(self.temperature_history),np.array(self.timestamps)))
            else:
                D = 0
                
            self.output = max(min(P+I+D,self.max_output),self.min_output)
            
            self.output_P = P
            self.output_I = I
            self.output_D = D
            #print(self.output_P, self.output_I, self.output_D)

        else:
            self.output = 0 
        
        return self.output
        