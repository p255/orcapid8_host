import state
import pid
#import sample

class Channel():
    def __init__(self, channel_index, spi):
        self.state = state.ChannelState(channel_index)
        self.pid = pid.PID()
        self.spi = spi
        #self.sample = sample.Sample()
    
    def update(self, command):
        # update command info
        self.state.command.__dict__.update(command.__dict__)
        
        # run PID step
        self.state.output = self.pid.step(self.state)
        
        # update TC readings and output voltage
        voltage = (self.state.output/100.)*0.81+0.095
        if not self.state.command.sw_enable:
            voltage = 0 # really zero this if software enable is zero; better noise tolerance
        self.state.TCA["ext"],self.state.TCA["int"],self.state.TCA["fault"],self.state.TCB["ext"],self.state.TCB["int"],self.state.TCB["fault"],self.state.hw_enable = self.spi.update_channel(self.state.channel_index-1, voltage)
        
        # hack: behavioral model
        #self.state.TCA["ext"] = self.sample.update(self.state.output)