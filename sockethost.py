import socket
import sockethelper as sh

import netifaces as ni

read_file = "pkl/state.pkl" 
write_file = "pkl/cmd.pkl" 

ni.ifaddresses('eth0')
ip = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
print ('host ip: ' + ip) 


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind the socket to the port
server_address = ('10.0.0.20', 25522)
print ('starting up on %s port %s' % server_address)
sock.bind(server_address)



# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)

        state = 0
        while True:
            data = connection.recv(1)
            if data == b'R':
                # client wants to read
                sh.send_file(connection,read_file)
                
            elif data == b'W':
                # client wants to write
                sh.recv_file(connection,write_file)

    finally:
        # Clean up the connection
        connection.close()
        
        
